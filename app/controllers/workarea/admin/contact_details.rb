module Workarea
  module Admin
    class ContactDetailsController < Admin::ApplicationController
      required_permissions :marketing
      def index
        @contact_details = Workarea::ContactDetail.all
      end
    end
  end
end
