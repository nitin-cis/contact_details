module Workarea
  module Storefront
    class ContactDetailsController < Storefront::ApplicationController
      # before_action :cache_page
      def new
        @contact_detail = Workarea::ContactDetail.new
      end

      def create
        @contact_detail = Workarea::ContactDetail.new(contact_detail_params)
        if @contact_detail.save
          flash[:success] = 'Blog has been created'
          redirect_to contact_detail_path(@contact_detail)
        else
          render :new
        end
      end

      def index
      end
      # def index
      #   blogs = Workarea::Content::Blog.all.map do |blog|
      #     Storefront::BlogViewModel.new(blog, view_model_options)
      #   end
      #   @blog_index = Storefront::BlogIndexViewModel.new(blogs, view_model_options)
      # end

      # def show
      #   model = Content::Blog.find_by(slug: params[:id])
      #   @blog = Storefront::BlogViewModel.new(model, view_model_options)
      # end

      def contact_detail_params
        return {} if params[:contact_detail].blank?
        params[:contact_detail].permit(:name, :email, :comments)
      end
    end
  end
end
