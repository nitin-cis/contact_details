module Workarea
  class ContactDetail
    include ApplicationDocument

    ATTRIBUTES_FOR_LENGTH_VALIDATION = [
      :name,
      :email,
      :comments
    ]

    field :name, type: String
    field :email, type: String
    field :comments, type: String

    # embedded_in :addressable, polymorphic: true
    validates :name, length: { maximum: 30 }
    validates :email, length: { maximum: 50 }
    # ATTRIBUTES_FOR_LENGTH_VALIDATION.each do |field|
    #   validates field, length: { maximum: 500 }
    # end
  end
end
